package com.example.boodschapbro.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GroceryList implements Serializable {
    public GroceryList() {
        items = new ArrayList<>();
        listName = "";
    }

    public String listName;
    public List<GroceryListItem> items;
    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GroceryListItem> getItems() {
        return items;
    }

    public void setItems(List<GroceryListItem> items) {
        this.items = items;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public void addItem(GroceryListItem item){
        items.add(item);
    }
}
