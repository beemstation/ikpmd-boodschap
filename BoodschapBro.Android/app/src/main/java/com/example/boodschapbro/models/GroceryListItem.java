package com.example.boodschapbro.models;

import java.io.Serializable;

public class GroceryListItem implements Serializable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String name;
    public int quantity;
    public Unit unit;
    public String id;

    public boolean isValid(){
        if(name == null || name.isEmpty()){
            return false;
        }
        if(quantity < 1){
            return false;
        }
        return true;
    }

    public enum Unit{
        Gr,
        Kg,
        Pcs,
    }
}