package com.example.boodschapbro.listoverview;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.boodschapbro.R;
import com.example.boodschapbro.adduser.AddUserToListActivity;
import com.example.boodschapbro.fragments.AddListDialog;
import com.example.boodschapbro.listdetail.ListDetailActivity;
import com.example.boodschapbro.models.GroceryList;
import com.example.boodschapbro.models.User;
import com.example.boodschapbro.settings.SettingsActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ListOverviewActivity extends AppCompatActivity implements AddListDialog.AddListDialogListener, GroceryListAdapter.OnGroceryListViewHolderClick  {
    private final String TAG = ListOverviewActivity.class.toString();
    private List<GroceryList> lists;
    private DatabaseReference userListParticipations;
    GroceryListAdapter groceryListAdapter;

    @Inject
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_overview);

        lists = new ArrayList<>();

        String userId = FirebaseAuth.getInstance().getUid();

        userListParticipations = database.getReference("user-list-participations").child(userId);

        ChildEventListener childListeners = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                int listSize = lists.size();
                String listName = snapshot.getValue(String.class);
                String key = snapshot.getKey();
                GroceryList list = new GroceryList();
                list.listName = listName;
                list.id = key;
                lists.add(list);
                groceryListAdapter.notifyItemInserted(listSize);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        userListParticipations.addChildEventListener(childListeners);
        //userReference.addListenerForSingleValueEvent(listener);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        groceryListAdapter = new GroceryListAdapter(this, lists, this);

        RecyclerView recyclerView = findViewById(R.id.listView_recyclerView);
        recyclerView.setAdapter(groceryListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void onAddButtonClicked(View view){
        showAddListDialog();
    }

    public void showAddListDialog(){
        DialogFragment addListModal = new AddListDialog();
        addListModal.show(getSupportFragmentManager(), "Add list");
    }

    @Override
    public void onAddListButtonClicked(String name) {
        if(name.isEmpty() || name == null){
            Toast.makeText(this, "Please enter a list name", Toast.LENGTH_SHORT).show();
            return;
        }
        String userId = FirebaseAuth.getInstance().getUid();
        GroceryList newGroceryList = new GroceryList();
        newGroceryList.listName = name;

        DatabaseReference listsReference = database.getReference("lists");
        DatabaseReference reference = database.getReference();
        String listKey = listsReference.push().getKey();
        newGroceryList.id = listKey;


        Map<String, Object> postValues = new HashMap<>();

        Map<String, Object> userList = new HashMap<>();
        userList.put(listKey, name);

        Map<String, Object> listUser = new HashMap<>();
        listUser.put(userId, true);

        Map<String, Object> userOwnsList = new HashMap<>();
        userOwnsList.put(listKey, true);

        postValues.put("user-list-participations/" + userId + "/" + listKey, name);
//        postValues.put("list-user-participations/" + listKey, listUser);
        postValues.put("lists/" + listKey, newGroceryList);
        postValues.put("ownslist/" + userId + "/" + listKey, true);

        reference.updateChildren(postValues)
                .addOnFailureListener(command -> {
                    Log.e(TAG,"Something went wrong while adding the list" + command.getMessage());
                    Toast.makeText(this, "Something went wrong while adding the list..", Toast.LENGTH_SHORT).show();
                });

        database.getReference("stats").child("lists").setValue(ServerValue.increment(1))
        .addOnFailureListener(command -> {
            Log.e(TAG,"something went wrong updating the stats: " + command.getMessage());
        });
    }

    @Override
    public void onGroceryListClicked(int index) {
        Intent intent = new Intent(this, ListDetailActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt("listIndex", index);
        bundle.putString("listId", lists.get(index).id);
        bundle.putString("listName", lists.get(index).listName);

        intent.putExtras(bundle);

        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(ListOverviewActivity.this, SettingsActivity.class));
                finish();
                return true;
            case R.id.settings_listOverview:
                startActivity(new Intent(ListOverviewActivity.this, AddUserToListActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}