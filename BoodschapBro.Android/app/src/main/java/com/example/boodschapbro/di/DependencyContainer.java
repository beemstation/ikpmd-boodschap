package com.example.boodschapbro.di;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class DependencyContainer {
    @Provides
    public FirebaseDatabase provideFirebaseDatabase(){
        return FirebaseDatabase.getInstance();
    }
}
