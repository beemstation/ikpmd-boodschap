package com.example.boodschapbro.adduser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.boodschapbro.R;
import com.example.boodschapbro.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class AddUserToListActivity extends AppCompatActivity {
    private final String TAG = AddUserToListActivity.class.toString();

    private RecyclerView recyclerView;
    private UserListAdapter adapter;

    private String listId;
    private String listName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_to_list);

        listId = getIntent().getExtras().getString("listId");
        listName = getIntent().getExtras().getString("listName");

        ArrayList<User> users = new ArrayList<>();

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                User user = snapshot.getValue(User.class);
                String uuid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if(uuid.equals( user.uuid))
                    return;
                int listSize = users.size();
                users.add(user);
                adapter.notifyItemInserted(listSize);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        FirebaseDatabase fbdb = FirebaseDatabase.getInstance();
        fbdb.getReference("users").addChildEventListener(listener);

        adapter = new UserListAdapter(users, this);

        adapter.addUserEventHandler = u -> {
            DatabaseReference reference = fbdb.getReference();
            Map<String, Object> postValues = new HashMap<>();

            Map<String, Object> userList = new HashMap<>();
            userList.put(listId, listName);

            Map<String, Object> listUser = new HashMap<>();
            listUser.put(u.uuid, true);

            postValues.put("user-list-participations/" + u.uuid, userList);
//            postValues.put("list-user-participations/" + listId, listUser);

            reference.updateChildren(postValues)
                    .addOnFailureListener(command -> {
                        Log.e(TAG,"Something went wrong while adding user to the list" + command.getMessage());
                        Toast.makeText(this, "Something went wrong while adding user to the list..", Toast.LENGTH_SHORT);
                    });
        };

        recyclerView = findViewById(R.id.recyclerview_users);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}