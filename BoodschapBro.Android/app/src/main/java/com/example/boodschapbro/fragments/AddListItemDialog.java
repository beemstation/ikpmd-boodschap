package com.example.boodschapbro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.boodschapbro.R;
import com.example.boodschapbro.models.GroceryListItem;


public class AddListItemDialog extends DialogFragment {
    private final String TAG = AddListItemDialog.class.toString();
    private EditText itemName_editText;
    private EditText itemQuantity_editText;
    private Spinner unitSelectSpinner;

    private TextView actionOk, actionCancel;

    public interface AddListItemDialogListener{
        void onAddListItemButtonClicked(GroceryListItem item);
    }

    private AddListItemDialogListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_list_item, container, false);
        unitSelectSpinner = (Spinner) view.findViewById(R.id.spinner_unit);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, getHumanReadableUnitStrings());
        unitSelectSpinner.setAdapter(adapter);

        actionCancel = view.findViewById(R.id.action_cancel);
        actionOk = view.findViewById(R.id.add_list_item_ok);
        itemName_editText = view.findViewById(R.id.add_list_inputItem);
        itemQuantity_editText = view.findViewById(R.id.editTextQuantity);

        itemName_editText.requestFocus();

        actionCancel.setOnClickListener(v -> getDialog().dismiss());
        actionOk.setOnClickListener(v -> {
            if(itemQuantity_editText.getText().toString().isEmpty()){
                getDialog().dismiss();
                Toast.makeText(getActivity(), "Please enter a valid amount..", Toast.LENGTH_SHORT).show();
                return;
            }
            GroceryListItem newItem = new GroceryListItem();
            newItem.setName(itemName_editText.getText().toString());
            newItem.setQuantity(Integer.parseInt(itemQuantity_editText.getText().toString()));
            newItem.setUnit(GroceryListItem.Unit.valueOf((String)unitSelectSpinner.getSelectedItem()));

            listener.onAddListItemButtonClicked(newItem);
            getDialog().dismiss();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddListItemDialogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach", e);
        }
    }
    
    private String[] getHumanReadableUnitStrings(){
        GroceryListItem.Unit[] units = GroceryListItem.Unit.class.getEnumConstants();
        String[] returnValues = new String[units.length];

        for (int i = 0; i < units.length; i++){
            returnValues[i] = units[i].toString();
        }
        return returnValues;
    }
}
