package com.example.boodschapbro.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.boodschapbro.MainActivity;
import com.example.boodschapbro.R;
import com.example.boodschapbro.models.Stats;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SettingsActivity extends AppCompatActivity {
    TextView usersValue;
    TextView listsValue;
    TextView itemsValue;

    @Inject
    public FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        this.usersValue = findViewById(R.id.textView_UsersValue);
        this.listsValue = findViewById(R.id.textView_listsValue);
        this.itemsValue = findViewById(R.id.textView_itemsValue);

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(!snapshot.exists()){
                    initializeStatsInDb();
                    return;
                }
                Stats stats = snapshot.getValue(Stats.class);
                setStatsValues(stats);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(SettingsActivity.this, "Something went wrong gathering the stats..", Toast.LENGTH_SHORT);
            }
        };

        database.getReference("stats").addValueEventListener(listener);
    }

    public void onLogoutClicked(View view){
        FirebaseAuth.getInstance().signOut();
        Intent logoutIntent = new Intent(this, MainActivity.class);
        logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(logoutIntent);
        finish();
    }
    private void initializeStatsInDb(){
        database.getReference("stats").setValue(new Stats());
    }
    private void setStatsValues(Stats stats){
        this.usersValue.setText(String.valueOf(stats.users));
        this.listsValue.setText(String.valueOf(stats.lists));
        this.itemsValue.setText(String.valueOf(stats.items));
    }
}