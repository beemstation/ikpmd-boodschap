package com.example.boodschapbro.listdetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boodschapbro.models.GroceryListItem;

import java.util.List;

import com.example.boodschapbro.R;

public class GroceryListItemAdapter extends RecyclerView.Adapter<GroceryListItemAdapter.GroceryListItemViewHolder> {
    private final Context context;
    private final List<GroceryListItem> listItems;

    public GroceryListItemAdapter(Context context, List<GroceryListItem> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public GroceryListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.card_grocery_list_item, parent, false);
        return new GroceryListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroceryListItemViewHolder holder, int position) {
        holder.listItemName.setText(listItems.get(position).name);
        holder.listItemQuantity.setText(Integer.toString(listItems.get(position).quantity));
        holder.listItemUnit.setText(listItems.get(position).unit.toString());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class GroceryListItemViewHolder extends RecyclerView.ViewHolder {
        TextView listItemName, listItemQuantity, listItemUnit;

        public GroceryListItemViewHolder(@NonNull View itemView) {
            super(itemView);
            listItemName = itemView.findViewById(R.id.textview_listItemName);
            listItemQuantity = itemView.findViewById(R.id.textView_listItemQuantity);
            listItemUnit = itemView.findViewById(R.id.textView_listItemUnit);
        }
    }

    public void deleteItem(int index){
        listItems.remove(index);
        notifyItemRemoved(index);
    }
}
