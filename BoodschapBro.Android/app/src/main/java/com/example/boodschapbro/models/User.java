package com.example.boodschapbro.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User {
    public String emailAddress;
    public String uuid;

    public User(String emailAddress, String uuid) {
        this.emailAddress = emailAddress;
        this.uuid = uuid;
    }

    public User() {
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
