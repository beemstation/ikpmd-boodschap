package com.example.boodschapbro.adduser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boodschapbro.R;
import com.example.boodschapbro.models.User;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserListItemViewHolder>  {
    List<User> users;
    Context context;
    OnAddUserClicked addUserEventHandler;

    public UserListAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @NonNull
    @Override
    public UserListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.card_userlist, parent, false);
        return new UserListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserListItemViewHolder holder, int position) {
        holder.userName.setText(users.get(position).getEmailAddress());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UserListItemViewHolder extends RecyclerView.ViewHolder {
        Button addUserButton;
        TextView userName;
        public UserListItemViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.userEmail);
            addUserButton = itemView.findViewById(R.id.addUserButton);
            addUserButton.setOnClickListener(v ->{
                addUserEventHandler.onAddUser(users.get(getAdapterPosition()));
            });
        }
    }

    public interface OnAddUserClicked{
        void onAddUser(User user);
    }
}
