package com.example.boodschapbro.listdetail;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.boodschapbro.R;
import com.example.boodschapbro.adduser.AddUserToListActivity;
import com.example.boodschapbro.fragments.AddListItemDialog;
import com.example.boodschapbro.models.GroceryListItem;
import com.example.boodschapbro.settings.SettingsActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ListDetailActivity extends AppCompatActivity implements AddListItemDialog.AddListItemDialogListener {
    private static final String TAG  = ListDetailActivity.class.toString();

    private List<GroceryListItem> listItems;
    private GroceryListItemAdapter adapter;
    private String listId;
    private String listName;
    private DatabaseReference listRef;

    @Inject
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_detail);

        listItems = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listId =  getIntent().getExtras().getString("listId");
        listName =  getIntent().getExtras().getString("listName");

        //setTitle(list.listName);
        listRef = database.getReference("lists").child(listId);
        ChildEventListener eventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                GroceryListItem item = snapshot.getValue(GroceryListItem.class);
                int listSize = listItems.size();
                listItems.add(item);
                adapter.notifyItemInserted(listSize);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                GroceryListItem list = snapshot.getValue(GroceryListItem.class);

                Optional<GroceryListItem> matchingObject = listItems.stream().
                        filter(p -> p.id.equals(list.id)).
                        findFirst();
                GroceryListItem otherList;
                if(!matchingObject.isPresent()){
                    Log.d("eek!", "error");
                    return;
                }
                otherList = matchingObject.get();

                int index = listItems.indexOf(otherList);
                listItems.remove(index);
                adapter.notifyItemRemoved(index);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        listRef.child("items").addChildEventListener(eventListener);
        adapter = new GroceryListItemAdapter(this, listItems);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter, index -> {
            GroceryListItem item = listItems.get(index);

            listRef.child("items").child(item.id).removeValue()
                .addOnFailureListener(command -> {
                    Toast.makeText(this, "Something went wrong accessing the database: " + command.getMessage(), Toast.LENGTH_SHORT);
                });
        }));
        RecyclerView recyclerView = findViewById(R.id.recyclerview_list_detail_view);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void onAddItemButtonClicked(View view){
        Log.d(TAG, "onAddItemButtonClicked clicked.");
        showAddListDialog();
    }

    public void showAddListDialog(){
        DialogFragment addListModal = new AddListItemDialog();
        addListModal.show(getSupportFragmentManager(), "Add list item");
    }

    @Override
    public void onAddListItemButtonClicked(GroceryListItem item) {
        if(!item.isValid()){
            Toast.makeText(this, "Please make sure you entered a correct product..", Toast.LENGTH_SHORT).show();
            return;
        }
        String key = listRef.child("items").push().getKey();
        item.id = key;
        listRef.child("items").child(key).setValue(item)
                .addOnFailureListener(command -> {
                    Toast.makeText(this, "Something went wrong accessing the database: " + command.getMessage(), Toast.LENGTH_SHORT).show();
                });

        database.getReference("stats").child("items").setValue(ServerValue.increment(1))
        .addOnFailureListener(command -> {
            Toast.makeText(this, "Something went wrong accessing the database: " + command.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_listoverview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(ListDetailActivity.this, SettingsActivity.class));
                finish();
                return true;


            case R.id.settings_listOverview:
                Intent intent = new Intent(ListDetailActivity.this, AddUserToListActivity.class);
                intent.putExtra("listId", listId);
                intent.putExtra("listName", listName);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback{
        private GroceryListItemAdapter adapter;
        private OnItemSwiped onItemSwiped;
        public SwipeToDeleteCallback(GroceryListItemAdapter adapter, OnItemSwiped onItemSwiped) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            this.adapter = adapter;
            this.onItemSwiped = onItemSwiped;
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            onItemSwiped.onItemSwiped(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public interface OnItemSwiped {
        void onItemSwiped(int index);
    }
}