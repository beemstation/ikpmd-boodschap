package com.example.boodschapbro.listoverview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boodschapbro.R;
import com.example.boodschapbro.models.GroceryList;

import java.util.HashMap;
import java.util.List;

public class GroceryListAdapter extends RecyclerView.Adapter<GroceryListAdapter.GroceryListViewHolder> {
    private final Context context;
    private final List<GroceryList> groceryLists;
    private OnGroceryListViewHolderClick onItemClickHandler;

    public interface OnGroceryListViewHolderClick{
        void onGroceryListClicked(int index);
    }

    public GroceryListAdapter(Context context, List<GroceryList> groceryLists, OnGroceryListViewHolderClick onItemClickHandler) {
        this.context = context;
        this.groceryLists = groceryLists;
        this.onItemClickHandler = onItemClickHandler;
    }

    @NonNull
    @Override
    public GroceryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.card_grocery_list_overview_row, parent, false);
        return new GroceryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroceryListViewHolder holder, int position) {
        holder.listName.setText(groceryLists.get(position).listName);
    }

    @Override
    public int getItemCount() {
        return groceryLists.size();
    }

    public class GroceryListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView listName;

        public GroceryListViewHolder(@NonNull View itemView) {
            super(itemView);
            listName = itemView.findViewById(R.id.groceryCardView_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickHandler.onGroceryListClicked(getAdapterPosition());
        }
    }
}
