package com.example.boodschapbro.register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.boodschapbro.MainActivity;
import com.example.boodschapbro.R;
import com.example.boodschapbro.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseAuth.getInstance().signOut();
    }

    public void onRegisterButtonClicked(View view){
        EditText emailText = findViewById(R.id.register_emailAddress);
        EditText passwordText = findViewById(R.id.register_password);
        EditText passwordRepeatText = findViewById(R.id.register_repeatPassword);
        if(!passwordRepeatText.getText().toString().equals(passwordText.getText().toString())){
            Toast.makeText(this, "The passwords don't match.", Toast.LENGTH_SHORT).show();
            return;
        }
        FirebaseAuth.getInstance().signOut();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailText.getText().toString(), passwordText.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String userId = task.getResult().getUser().getUid();
                            String userEmail = task.getResult().getUser().getEmail();
                            Log.d("created", "createUserWithEmail:success");
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            User user = new User();
                            user.uuid = userId;
                            user.emailAddress = userEmail;

                            DatabaseReference ref = database.getReference();
                            DatabaseReference userRef = ref.child("users").child(userId);
                            Map<String, User> users = new HashMap<>();
                            userRef.setValue(user).addOnFailureListener(command -> Log.d("ERROR WHILE ADDING USER", command.getMessage()));


                            database.getReference("stats").child("users").setValue(ServerValue.increment(1));
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            finish();

                        } else {
                            Log.w("", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}