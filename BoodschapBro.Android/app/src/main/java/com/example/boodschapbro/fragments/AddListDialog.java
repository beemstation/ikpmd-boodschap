package com.example.boodschapbro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.boodschapbro.R;

public class AddListDialog extends DialogFragment {
    private String TAG = AddListDialog.class.toString();

    private EditText editText;
    private TextView actionOk, actionCancel;

    public interface AddListDialogListener{
        void onAddListButtonClicked(String name);
    }

    private AddListDialogListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_list, container, false);

        actionCancel = view.findViewById(R.id.action_cancel);
        actionOk = view.findViewById(R.id.add_list_item_ok);
        editText = view.findViewById(R.id.add_list_inputItem);
        editText.requestFocus();

        actionCancel.setOnClickListener(v -> getDialog().dismiss());
        actionOk.setOnClickListener(v -> {
            String text = editText.getText().toString();
            if(text.isEmpty() || text == null){
                Toast.makeText(view.getContext(), "Please enter a list name", Toast.LENGTH_SHORT).show();
                return;
            }
            listener.onAddListButtonClicked(editText.getText().toString());
            getDialog().dismiss();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddListDialogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach", e);
        }
    }
}