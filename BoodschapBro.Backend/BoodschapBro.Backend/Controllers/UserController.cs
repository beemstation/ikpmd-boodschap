﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoodschapBro.Backend.Models;
using BoodschapBro.Backend.Models.ViewModels;
using BoodschapBro.Backend.Services;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BoodschapBro.Backend.Controllers
{
    [Route("user")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IFirebaseService _firebaseService;
        private readonly ILogger<UserController> _logger;

        public UserController(IFirebaseService firebaseService, ILogger<UserController> logger)
        {
            _firebaseService = firebaseService;
            _logger = logger;
        }
        
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [HttpPost]
        [Route("authenticate")]
        public IActionResult Authenticate([FromBody] LoginRequest loginRequest)
        {
            return new OkObjectResult("lols");
        }

        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] UserRegisterRequest userRegisterRequest)
        {
            try
            {
                await _firebaseService.CreateUser(userRegisterRequest);
            }
            catch (AggregateException e)
            {
                _logger.LogError(e, "Error occured while authenticating with Firebase: ");
                return new StatusCodeResult(500);
            }

            return new OkResult();
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> FindUsers()
        {
            return new OkResult();
        }
    }
}