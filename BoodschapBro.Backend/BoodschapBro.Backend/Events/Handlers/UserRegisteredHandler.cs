﻿using System.Threading;
using System.Threading.Tasks;
using BoodschapBro.Backend.Services;
using MediatR;
using Microsoft.Extensions.Logging;

namespace BoodschapBro.Backend.Events.Handlers
{
    public class UserRegisteredHandler : INotificationHandler<UserRegisteredNotification>
    {
        private readonly IFirebaseService _firebaseService;
        private readonly ILogger<UserRegisteredHandler> _logger;

        public UserRegisteredHandler(IFirebaseService firebaseService, ILogger<UserRegisteredHandler> logger)
        {
            _logger = logger;
            _firebaseService = firebaseService;
        }

        public Task Handle(UserRegisteredNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"notification: {notification.User.EmailAddress}");
            return Task.CompletedTask;
        }
    }
}