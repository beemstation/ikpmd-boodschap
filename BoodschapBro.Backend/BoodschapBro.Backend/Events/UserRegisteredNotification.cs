﻿using BoodschapBro.Backend.Models.BusinessEntities;
using MediatR;

namespace BoodschapBro.Backend.Events
{
    public class UserRegisteredNotification : INotification
    {
        public readonly User User;

        public UserRegisteredNotification(User user)
        {
            User = user;
        }
    }
}