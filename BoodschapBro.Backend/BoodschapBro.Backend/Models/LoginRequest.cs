﻿using System.ComponentModel.DataAnnotations;

namespace BoodschapBro.Backend.Models
{
    public class LoginRequest
    {
        [EmailAddress]
        [Required]
        public string EmailAddress { get; set; }
        [MinLength(12)]
        [Required]
        public string Password { get; set; }
    }
}