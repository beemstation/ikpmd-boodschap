﻿namespace BoodschapBro.Backend.Models.BusinessEntities
{
    public class User
    {
        public string EmailAddress { get; set; }
        public string UserName { get; set; }
        public string Uid { get; set; }
    }
}