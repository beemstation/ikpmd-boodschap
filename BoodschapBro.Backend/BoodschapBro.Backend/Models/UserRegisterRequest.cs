﻿using System.ComponentModel.DataAnnotations;

namespace BoodschapBro.Backend.Models
{
    public class UserRegisterRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public string Password { get; set; }
    }
}