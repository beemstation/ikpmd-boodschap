﻿using System.Threading.Tasks;
using BoodschapBro.Backend.Models.BusinessEntities;

namespace BoodschapBro.Backend.Services
{
    public interface IUserService
    {
        public Task<User> CreateUser(User user);
    }
}