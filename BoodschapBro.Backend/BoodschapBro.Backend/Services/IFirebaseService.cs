﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoodschapBro.Backend.Models;
using BoodschapBro.Backend.Models.ViewModels;

namespace BoodschapBro.Backend.Services
{
    public interface IFirebaseService
    {
        public Task CreateUser(UserRegisterRequest request);
    }
}