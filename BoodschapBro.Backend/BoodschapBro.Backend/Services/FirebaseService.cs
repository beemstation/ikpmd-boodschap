﻿using System.Threading.Tasks;
using BoodschapBro.Backend.Events;
using BoodschapBro.Backend.Models;
using BoodschapBro.Backend.Models.BusinessEntities;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using MediatR;

namespace BoodschapBro.Backend.Services
{
    public class FirebaseService : IFirebaseService
    {
        private readonly FirebaseApp _firebaseApp;
        private readonly IMediator _mediator;
        
        public FirebaseService(IMediator mediator)
        {
            _mediator = mediator;
            _firebaseApp =  FirebaseApp.Create(new AppOptions
            {
                Credential = GoogleCredential.FromFile("./firebase-auth.json")
            });
        }

        public async Task CreateUser(UserRegisterRequest request)
        {
            UserRecordArgs args = new UserRecordArgs
            {
                Email = request.EmailAddress,
                Password = request.Password,
                DisplayName = request.UserName
            };
            
            UserRecord user = await FirebaseAuth.DefaultInstance.CreateUserAsync(args);
            
            var userEntity = new User
            {
                Uid = user.Uid,
                EmailAddress = user.Email,
                UserName = user.DisplayName
            };
            
            await _mediator.Publish(new UserRegisteredNotification(userEntity));
        }
    }
}